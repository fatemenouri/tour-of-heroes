import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TourOfHerosComponent } from './components/tour-of-heros/tour-of-heros.component';

const routes: Routes = [
  { path: '', component: TourOfHerosComponent },
  { path: 'heroes', component: TourOfHerosComponent },
  { path: 'heroes/id', component: TourOfHerosComponent },
  { path: 'dashboard', component: TourOfHerosComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
