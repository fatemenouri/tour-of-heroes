import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TourOfHerosComponent } from './tour-of-heros.component';

describe('TourOfHerosComponent', () => {
  let component: TourOfHerosComponent;
  let fixture: ComponentFixture<TourOfHerosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TourOfHerosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TourOfHerosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
