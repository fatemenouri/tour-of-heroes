import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-tour-of-heros',
  templateUrl: './tour-of-heros.component.html',
  styleUrls: ['./tour-of-heros.component.scss']
})
export class TourOfHerosComponent implements OnInit {

  constructor(
    private activatedRoute: ActivatedRoute
  ) {
    this.activatedRoute.params.subscribe(
      params => {
        console.log(params.id)
      }
    )
  }

  ngOnInit(): void {
    this.dashboard = false;
    this.herolist=false;
  }

  heroesList: any = [
    { id: 11, name: 'Dr Nice' },
    { id: 12, name: 'Narco' },
    { id: 13, name: 'Bombasto' },
    { id: 14, name: 'Celeritas' },
    { id: 15, name: 'Magneta' },
    { id: 16, name: 'RubberMan' },
    { id: 17, name: 'Dynama' },
    { id: 18, name: 'Dr IQ' },
    { id: 19, name: 'Magma' },
    { id: 20, name: 'Tornado' }
  ];

  selectedHero?: any;
  onSelect(hero: any): void {
    this.selectedHero = hero;
  }
  dashboard: boolean = false;
  topHeroes(): void {
    this.heroesList = this.heroesList.slice(1, 5);
    this.dashboard = !this.dashboard;
  }
  herolist: boolean = false;
  heroList(): void {
    this.herolist = !this.herolist;
  }




}

